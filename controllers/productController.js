const Product = require("../models/Product.js");
const User = require("../models/User.js");

const bcrypt = require("bcrypt");

const auth = require("../auth");

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
};

module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
};

module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}


module.exports.addProduct = (data) => {
	if (data.isAdmin || data.isSeller) {
		let newProduct = new Product({
			productName : data.product.name,
			productDescription : data.product.description,
			productPrice : data.product.price,
			stock: data.product.stock,
			isActive: data.product.isActive,
			pro: [{ player: data.product.player }]
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	} 
	// User is not an admin or seller
	let message = Promise.resolve(`User must be ADMIN or SELLER to access this!`);
	return message.then((value) => {
		return {value}
	})
};

module.exports.updateProduct = (data, reqParams, reqBody) => {
	if (data.isAdmin || data.isSeller) {
		let updatedProduct = {};

		if (reqBody.name) {
			updatedProduct.productName = reqBody.name;
		}

		if (reqBody.description) {
			updatedProduct.productDescription = reqBody.description;
		}

		if (reqBody.price) {
			updatedProduct.productPrice = reqBody.price;
		}

		if (reqBody.stock) {
			updatedProduct.stock = reqBody.stock;
		}

		if (reqBody.player) {
			updatedProduct.$push = {pro: {player: reqBody.player}};
		}

		if (reqBody.isActive !== undefined) {
			updatedProduct.isActive = reqBody.isActive;
		}

		return Product.findByIdAndUpdate(reqParams.productId, { $set: updatedProduct }).then((product,error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	}
	let message = Promise.resolve(`User must be ADMIN or SELLER to access this!`);
	return message.then((value) => {
		return {value}
	});
};

module.exports.archiveProduct = (data) => {
	if (data.isAdmin || data.isSeller) {
		let archiveProduct = {
			isActive: false
		};
		return Product.findByIdAndUpdate(data.productId, archiveProduct).then((product,error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	};	
	// User is not an admin
	let message = Promise.resolve(`User must be ADMIN or SELLER to access this!`);
	return message.then((value) => {
		return {value}
	});
}


module.exports.enableProduct = (data) => {
	if (data.isAdmin || data.isSeller) {
		let archiveProduct = {
			isActive: true
		};
		return Product.findByIdAndUpdate(data.productId, archiveProduct).then((product,error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		})
	};	
	// User is not an admin
	let message = Promise.resolve(`User must be ADMIN or SELLER to access this!`);
	return message.then((value) => {
		return {value}
	});
}

module.exports.addToCart = async (data) => {
	if (!data.isAdmin) {
		try {
			// Check if the product is active
			const product = await Product.findById(data.productId);
			if (!product.isActive){
				return `Product is not active`
			}

			// Add or update product details in cart of user
			const user = await User.findById(data.userId);
			const productDetails = await Product.findById(data.productId);
			const existingCartItem = user.cart.find(item => item.productId.toString() === data.productId);
			if (existingCartItem) {
				existingCartItem.quantity += data.quantity;
				existingCartItem.subtotal = existingCartItem.productPrice * existingCartItem.quantity;
			} else {
				const cartItem = {
					productId: productDetails._id,
					productName: productDetails.productName,
					productPrice: productDetails.productPrice,
					quantity: data.quantity,
					subtotal: productDetails.productPrice * data.quantity // calculate subtotal based on quantity and productPrice
				};
				user.cart.push(cartItem);
			}

			const cartGrandTotal = user.cart.reduce((total, item) => total + item.subtotal, 0);
			user.cartGrandTotal = cartGrandTotal;
			const updatedUser = await user.save();

			return updatedUser;
		} catch (error) {
			console.error(error);
			return false;
		}
	}
	let message = Promise.resolve(`User must NOT be ADMIN to access this!`);
	return message.then((value) => {
		return {value}
	});
}



