const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const productController = require("../controllers/productController.js");


const auth = require("../auth");



router.get("/all", (req,res) => {
	productController.getAllProducts().then(
		resultFromController => res.send(resultFromController)
		);
});

router.get("/active", (req,res) => {
	productController.getActiveProducts().then(
		resultFromController => res.send(resultFromController)
		);
});

router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(
		resultFromController => res.send(resultFromController)
		);
});

router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		isSeller: auth.decode(req.headers.authorization).isSeller,
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


router.patch("/:productId/update", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		isSeller: auth.decode(req.headers.authorization).isSeller
	}
	productController.updateProduct(data, req.params, req.body).then(
		resultFromController => res.send(resultFromController)
		);
});

router.patch("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		isSeller: auth.decode(req.headers.authorization).isSeller
	}
	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

router.patch("/:productId/enable", auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		isSeller: auth.decode(req.headers.authorization).isSeller
	}
	productController.enableProduct(data).then(resultFromController => res.send(resultFromController));
});

router.post("/addToCart", auth.verify, (req,res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addToCart(data).then(resultFromController => res.send(resultFromController));
});





/*=============================*/
	module.exports = router;
/*=============================*/