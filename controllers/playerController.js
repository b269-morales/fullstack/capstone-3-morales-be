const Player = require("../models/Player.js");

const User = require("../models/User.js");

const bcrypt = require("bcrypt");

const auth = require("../auth");


module.exports.getAllPlayers = () => {
	return Player.find({}).then(result => {
		return result;
	})
};


module.exports.addPlayer = (data) => {
	if (data.isAdmin) {
		let newPlayer = new Player({
			name: data.name
		});
		return newPlayer.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	} 
	// User is not an admin or seller
	let message = Promise.resolve(`User must be ADMIN or SELLER to access this!`);
	return message.then((value) => {
		return {value}
	})
};