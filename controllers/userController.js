const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");

const bcrypt = require("bcrypt");

const auth = require("../auth");


module.exports.getAllUser = () => {
	return User.find({})
    .populate("cart.productId", ["name", "price"]) // https://www.geeksforgeeks.org/mongoose-populate-method/
    .then((users) => {
      // REHASH OF OLD TOPIC: https://www.w3schools.com/jsref/jsref_map.asp
    	return users.map((user) => {
    		const userObj = user.toObject();
    		userObj.cartGrandTotal = user.cart.reduce(
    			(total, item) => total + item.subtotal,
    			0
    			);
    		userObj.orderGrandTotal = user.orders.reduce(
    			(total, item) => total + item.subtotal,
    			0
    			);
    		return userObj;
    	});
    });
};

module.exports.getProfile = (data) =>{
	if (data.isAdmin) 
	{
		return User.findById(data.user).then(result =>{
			result.password = "";
			return result;
		})
	}
	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) => {
		return {value}
	})
};

module.exports.getDetails = (data) =>{
		return User.findById(data.user).then(result =>{
			result.password = "";
			return result;
		})
};


module.exports.registerUser = (reqBody) => {
	return User.find({ $or: [{ email: reqBody.email}, {mobileNo: reqBody.mobileNo}]}).then((result) => {
		if (result.length > 0)
		{	
			return { error: "Email or mobile number is already registered" };
		} else {
			let newUser = new User ({
				userName: reqBody.userName,
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				mobileNo: reqBody.mobileNo,
				isSeller: reqBody.isSeller,
				password: bcrypt.hashSync(reqBody.password, 15) 
			});

			return newUser.save().then((user,error) => {
				if (error){
					return false;
				} else {
					return true;
				};
			})
		};

	})
};


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>
	{
		if (result == null){
			return false;
		} else {

			// EASIER IMPLEMENTATION: https://www.npmjs.com/package/bcrypt#usage-nodejs
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect){
				return {access: auth.createAccessToken(result)
			}
		} else {
			return false;
		}
	}
})
};


module.exports.updateUserAccess = (data, reqBody) => {
	if (data.isAdmin) {
		return User.findOne({ email: reqBody.email })
		.then((user) => {
			let updatedUserAccess = {};

			if (reqBody.admin) {
				updatedUserAccess.isAdmin = reqBody.admin;
			}

			if (reqBody.seller) {
				updatedUserAccess.isSeller = reqBody.seller;
			}


			return User.findByIdAndUpdate(user._id, { $set: updatedUserAccess }).then((user) => {
				if (user) {
					return `User access succesfully updated`;
				} else {
					return false;
				}
			});
		})
	}
	let message = Promise.resolve(`User must be ADMIN to access this!`);
	return message.then((value) => {
		return {value}
	});
};


module.exports.getCart = (data) => {
	return User.findById(data).then(result => {
		return { cart: result.cart, cartGrandTotal: result.cartGrandTotal };
	});
};

module.exports.getOrders = (data) => {
	return User.findById(data).then(result => {
		return { orders: result.orders, orderGrandTotal: result.orderGrandTotal };
	});
};



module.exports.updateCartItemQuantity = async (data) => {
	try {
		// BUGFIXING: DON'T KNOW WHY new ObjectId keeps getting returned by the cart.productId
		const user = await User.findById(data.user);
		if (!user || !user.cart || !data.productId) {
			return `Data Issue`;
		}
		const cartItem = user.cart.find(item => item.productId === data.productId);
		console.log (user);
		console.log(`cartItem: ${cartItem}`);
		if (!cartItem) {
			return `Product is not in cart`;
		}
		// FIX: I used mongoose.Schema.Types.ObjectId = switched it back to String for the user's orders.
		// retrieve the product from database so that the price is still dynamic
		// LIMITATION: only edits the price with quantity edits. Possible implementation in the future for the products to be updated with the same quantity in order to update the price when there are price changes
		const product = await Product.findById(data.productId);
		if (!product) {
			return `Invalid product`;
		}
		cartItem.quantity = data.quantity;
		cartItem.subtotal = product.productPrice * data.quantity;

		const cartGrandTotal = user.cart.reduce((total, item) => total + item.subtotal, 0);
		user.cartGrandTotal = cartGrandTotal;
		const updatedUser = await user.save();

		return updatedUser;
	} catch (error) {
		console.error(error);
		return false;
	}
};



module.exports.removeFromCart = async (userId, cartItemId) => {
	try {
		const user = await User.findById(userId);
		const indexToRemove = user.cart.findIndex((item) => item._id.toString() === cartItemId);
		if (indexToRemove === -1) {
			return `Item not found in cart`;
		}

		// SUPER HELPFUL LINK: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Subtraction_assignment
		user.cartGrandTotal -= user.cart[indexToRemove].subtotal;
		user.cart.splice(indexToRemove, 1);
		
		const updatedUser = await user.save();

		return updatedUser;
	} catch (error) {
		return `An error occurred while removing the item from the cart.`
	}
};


module.exports.addToOrders = async (data) => {
	if (!data.isAdmin){
		try {
			const user = await User.findById(data.userId).populate("cart.productId");

			// REHASH OF OLD TOPIC: https://www.w3schools.com/jsref/jsref_map.asp
			// Search for possible other implementations as this leaves too much room for error 
			// FIXED (will do for now)
			const products = user.cart.map((item) => ({
				productId: item.productId,
				productName: item.productName,
				quantity: item.quantity,
				subtotal: item.subtotal,
				orderedOn: new Date(),
			})
			);

			const totalAmount = user.cartGrandTotal;
			const order = await Order.create({ userId: data.userId, products, totalAmount });

			user.orders.push({
				orderId: order._id,
				productName: products.map((product) => product.productName).join(", "),
				quantity: products.reduce((acc, product) => acc + product.quantity, 0),
				subtotal: totalAmount,
				orderedOn: new Date(),
				status: "Ordered",
			});

			// Deduct stock from product schema (WIP?)
			// GOOD DOCUMENTATION: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of
			// FIXED
			for (const item of user.cart) {
				const product = await Product.findById(item.productId);
				product.stock -= item.quantity;
				await product.save();
			}
			// Empty the user's cart (WIP?)
			// FIXED
			user.cart = [];
			user.cartGrandTotal = 0;

			// Update the orders' grand total for the user (WIP)
			// FIXED 
			user.orderGrandTotal += totalAmount;


			await user.save();

			return order;
		} catch (err) {
			console.error(err);
			return false;
		}
	} let message = Promise.resolve(`User must NOT be ADMIN to access this!`);
	return message.then((value) => {
		return {value}
	});
};




